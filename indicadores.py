from area_responsable import AreaResponsable
from tipo_de_obra import TipoDeObra
from obra import Obra
from peewee import *
from adjudicacion import Adjudicacion
from etapa import Etapa
from barrio import Barrio
from comuna import Comuna
from ubicacion import Ubicacion

# Cantidad de obras que se encuentran en cada etapa
def etapas_obras():
    resultado = {}
    obras_por_etapa = (
        Obra
        .select(Obra.etapa_id, fn.COUNT(Obra.id).alias('cantidad'))
        .group_by(Obra.etapa_id)
    )

    for obra_etapa in obras_por_etapa:
        nombre_etapa = Etapa.get(Etapa.id == obra_etapa.etapa_id).nombre
        resultado[nombre_etapa] = obra_etapa.cantidad

    print('\n|=========| Cantidad de obras que se encuentran en cada etapa |==========|')
    for nombre_etapa, cantidad_obras in resultado.items():
        print(f'{nombre_etapa}: {cantidad_obras}')

# Cantidad de obras y monto total de inversion por tipo de obra
def monto_inversiones_por_tipo_obra():
    resultado = {}

    obras_por_tipo = (
        Obra
        .select(Obra.tipo_de_obra, fn.COUNT(Obra.id).alias('cantidad'))
        .group_by(Obra.tipo_de_obra)
    )

    for tipo in obras_por_tipo:
        tipo_obra = TipoDeObra.get(TipoDeObra.id == tipo.tipo_de_obra).nombre

        monto_total = (
            Adjudicacion
            .select(fn.SUM(Adjudicacion.monto_contrato).alias('monto_total'))
            .join(Obra, on=(Adjudicacion.id == Obra.adjudicacion_id))
            .where(Obra.tipo_de_obra == tipo.tipo_de_obra)
            .scalar() or 0
        )

        resultado[tipo_obra] = {'cantidad': tipo.cantidad, 'monto_total': monto_total}
    print('\n|=========| Cantidad de obras y monto total de inversion por tipo de obra |==========|')

    for tipo_obra, info in resultado.items():
        print(f'{tipo_obra} - Cantidad de obras: {info["cantidad"]} - Monto total: {info["monto_total"]}')

# Listado de todos los barrios pertenecientes a las comunas 1, 2 y 3.
def ver_barrios_por_comunas(comunas_seleccionadas):
    resultado = {}
    ids_comunas = (Comuna.select(Comuna.numero, Comuna.id).where(Comuna.numero.in_(comunas_seleccionadas)))

    for comuna in ids_comunas:
        nombres_barrios = (Barrio.select(Barrio.nombre).where(Barrio.comuna_id == comuna.id))
        resultado[f'Comuna {comuna.numero}'] = [barrio.nombre for barrio in nombres_barrios]

    print('\n|=========| Barrios correspondientes a las comunas 1, 2 y 3 |==========|')

    for comuna, barrios in resultado.items():
        print(f'{comuna}: {barrios}')

# Cantidad de obras finalizadas y su monto total de inversión en la comuna 1.
def obtener_cantidad_obras_y_monto_total_en_comuna(comuna_numero):
    cantidad_obras_finalizadas = (
        Obra
        .select(fn.COUNT(Obra.id).alias('cantidad_obras'))
        .where(Obra.porcentaje_avance == 100)
        .scalar() or 0
    )

    ids_obras_finalizadas = (
        Obra
        .select(Obra.id)
        .where(Obra.porcentaje_avance == 100)
    )

    ids_ubicaciones = (
        Ubicacion
        .select(Ubicacion.id)
        .join(Obra, on=(Ubicacion.id == Obra.ubicacion_id))
        .where(Obra.id.in_(ids_obras_finalizadas))
    )

    ids_barrios_en_comuna = (
        Barrio
        .select(Barrio.id)
        .where(Barrio.comuna_id == comuna_numero)
    )

    ids_obras_en_barrios = (
        Obra
        .select(Obra.id)
        .where(Obra.ubicacion_id.in_(ids_ubicaciones) & Obra.adjudicacion_id.is_null(False))
    )

    monto_total = (
        Adjudicacion
        .select(fn.SUM(Adjudicacion.monto_contrato).alias('monto_total'))
        .join(Obra, on=(Adjudicacion.id == Obra.adjudicacion_id))
        .where(Obra.id.in_(ids_obras_en_barrios))
        .scalar() or 0
    )
    print('\n|=========| Cantidad de obras finalizadas y monto de inversion en comuna 1 |==========|')
    return cantidad_obras_finalizadas, monto_total

# Cantidad de obras finalizadas en un plazo menor o igual a 24 meses.
def obras_terminadas_en_plazo():
    cantidad_obras = (
        Obra
        .select(fn.COUNT(Obra.id).alias('cantidad_obras'))
        .where((Obra.porcentaje_avance == 100) & (Obra.plazo_meses <= 24))
        .scalar() or 0
    )
    print('\n|=========| Obras terminadas en un plazo de 24 meses o menos |==========|')
    return cantidad_obras

# Porcentaje total de obras finalizadas:
def calcular_porcentaje_obras_finalizadas():
    total_obras = Obra.select(fn.COUNT(Obra.id)).scalar() or 0
    obras_finalizadas = Obra.select(fn.COUNT(Obra.id)).where(Obra.porcentaje_avance == 100).scalar() or 0
    porcentaje_finalizadas = int((obras_finalizadas / total_obras) * 100) if total_obras > 0 else 0
    print('\n|=========| Porcentaje de obras finalizadas |==========|')
    return porcentaje_finalizadas

# Cantidad de mano de obra empleada:
def sumar_mano_de_obra_empleada():
    suma_mano_de_obra = (
        Obra
        .select(fn.SUM(Obra.mano_obra).alias('suma_mano_de_obra'))
        .scalar() or 0
    )
    print('\n|=========| Cantidad total de mano de obra empleada |==========|')
    return suma_mano_de_obra

def sumar_monto_total_de_inversion():
    sumar_monto_de_inversion = (
        Adjudicacion
        .select(fn.SUM(Adjudicacion.monto_contrato).alias('monto_total'))
        .scalar() or 0
    )
    print('\n|=========| Monto total de inversion |==========|')
    return sumar_monto_de_inversion

def ejecutar_indicadores():
    areas = AreaResponsable.ver_areas_responsables()
    for area in areas:
        print(area)

    tipos_obras = TipoDeObra.ver_tipo_de_obras()
    for tipo in tipos_obras:
        print(tipo)

    etapas_obras()
    monto_inversiones_por_tipo_obra()
    ver_barrios_por_comunas([1, 2, 3])

    comuna_numero = 1
    cantidad_obras, monto_total = obtener_cantidad_obras_y_monto_total_en_comuna(comuna_numero)

    print(f'Cantidad de obras finalizadas en la comuna {comuna_numero}: {cantidad_obras}')
    print(f'Monto total de inversion en obras finalizadas en la comuna {comuna_numero}: {monto_total}')
    cantidad_obras_terminadas = obras_terminadas_en_plazo()
    print(f'Cantidad de obras terminadas en 24 meses o menos: {cantidad_obras_terminadas}')
    porcentaje_finalizadas = calcular_porcentaje_obras_finalizadas()
    print(f'Porcentaje de obras finalizadas: {porcentaje_finalizadas}%')

    total_mano_de_obra = sumar_mano_de_obra_empleada()
    print(f'Total de mano de obra empleada: {total_mano_de_obra}')
    total_mano_de_obra = sumar_mano_de_obra_empleada()
    print(f'Total de mano de obra empleada: {total_mano_de_obra}')

#ejecutar_indicadores()    
