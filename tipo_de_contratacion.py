from peewee import AutoField, CharField
from base_model import BaseModel

class TipoDeContratacion(BaseModel):
    id = AutoField()
    nombre = CharField()

    class Meta:
        table_name = 'tipos_de_contratacion'

    def obtener_tipos_de_contratacion():
        tipos_de_contratacion = []
        tipos_de_contratacionrnos_en_bd = TipoDeContratacion.select(TipoDeContratacion.id, TipoDeContratacion.nombre)
        for tipo_de_contratacion in tipos_de_contratacionrnos_en_bd:
            plantilla = {
                'id': tipo_de_contratacion.id,
                'nombre': tipo_de_contratacion.nombre
            }
            tipos_de_contratacion.append(plantilla)
        return tipos_de_contratacion