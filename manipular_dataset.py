"""
https://medium.com/geekculture/dealing-with-null-values-in-pandas-dataframe-1a67854fe834
"""

import pandas as pd
from constantes import *
import sqlite3

def importar_datos_csv(archivo_csv: str):
    try:
        df = pd.read_csv(archivo_csv, sep=',')
        return df
    except FileNotFoundError as e:
        print('Error al conectar con el dataset.', e)


def importar_datos_csv_columnas(archivo_csv: str):
    try:
        df = pd.read_csv(archivo_csv, usecols=COLUMNAS_ELEGIDAS)
        return df
    except FileNotFoundError as e:
        print('Error al conectar con el dataset.', e)


#data = importar_datos_csv(DATASET).to_html()

# crear un archivo json con la data importada
# try:
#     file = open('data.html', 'xt')
#     file.write(data)
#     file.close()
# except FileExistsError as e:
#     print('No se puede crear el archivo.', e)

#raw_data = sqlite3.connect('./database/raw_data.db')
#bd_sql = importar_datos_csv_columnas(DATASET).to_sql('data', raw_data)


def verificar_info_df(data: pd.DataFrame):
    print('INFO: ', data.info())
    print('REGISTROS TOTALES: ', data.index)

# PRUEBAS

# importamos el dataset csv y lo convertimos en dataframe
data = importar_datos_csv_columnas(DATASET)
verificar_info_df(data)


# eliminar registros en columnas de valores no nulos
def quitar_nan_en_columnas(dataframe: pd.DataFrame, columnas: list):
    data_actualizada = dataframe.dropna(subset=columnas)
    return data_actualizada

data_actualizada = quitar_nan_en_columnas(data, COLUMNAS_CON_REGISTROS_NULOS_A_ELIMINAR)
print('DROPNA EN LISTADO DE COLUMNAS', data_actualizada)
#verificar_info_df(data_actualizada)


# modificar valores NaN de una columna en especifico
def cambiar_nan(dataframe: pd.DataFrame):
    data_actualizada = dataframe.fillna(value={ 'financiamiento': 'hola mundo' })
    return data_actualizada

#data_actualizada = cambiar_nan(data_actualizada)
#print('FILLNA EN COLUMNA ESPECIFICA: ', data_actualizada)
#verificar_info_df(data_actualizada)


# modificar valores NaN de varias columnas
def cambiar_nan_columnas(dataframe: pd.DataFrame, columnas: dict):
    data_actualizada = dataframe.fillna(value=columnas)
    return data_actualizada

data_actualizada = cambiar_nan_columnas(data_actualizada, VALORES_NULOS_A_REEMPLAZAR)
print('FILLNA EN VARIAS COLUMNAS: ', data_actualizada)
verificar_info_df(data_actualizada)


# eliminar filas con valor NaN en columna específica
def quitar_nan_columna(dataframe: pd.DataFrame):
    data_actualizada = dataframe.dropna(subset=['tipo'])
    return data_actualizada

#data_actualizada = quitar_nan_columna(data_actualizada)
#print('DROPNA EN COLUMNA ESPECIFICA', data_actualizada)
#verificar_info_df(data_actualizada)


# puedo quitar registros con multiples valores en la columna comunas

# df = data_actualizada
# df.drop(df[df['comuna'].compare])
# print(df)


prueba = importar_datos_csv_columnas(DATASET)
unicos = prueba['entorno'].str.lower().drop_duplicates()
print(unicos)