from peewee import AutoField, CharField
from base_model import BaseModel

class Entorno(BaseModel):
    id = AutoField()
    nombre = CharField(max_length=100)

    class Meta: 
        table_name = 'entornos'

    def obtener_entornos():
        entornos = []
        entornos_en_bd = Entorno.select(Entorno.id, Entorno.nombre)
        for entorno in entornos_en_bd:
            plantilla = {
                'id': entorno.id,
                'nombre': entorno.nombre
            }
            entornos.append(plantilla)
        return entornos