from gestionar_obra_publica import crear_bd
from interfaz_de_usuario import ejecutar_programa
import os
from bd import RUTA as ruta_bd

if os.path.exists(ruta_bd):
    print('La BD existe')
else:
    print('Se inicia proceso de creación de la BD')
    crear_bd()

ejecutar_programa()