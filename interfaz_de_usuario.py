from obra import Obra
from constantes import *
from gestionar_obra_publica import GestionarObraPublica
from indicadores import ejecutar_indicadores

def ejecutar_opcion_ingresada(opcion:int):
    match opcion:
        case 1: 
            print('\n|=========| Iniciar nuevo proyecto |==========|')
            try:
                GestionarObraPublica.nueva_obra()
            except:
                print(ERROR_GENERICO)
                  
        case 2:
            print('\n|=========| Insertar nueva contratacion |==========|')
            try:
                Obra.iniciar_contratacion()
            except:
                print(ERROR_GENERICO)

        case 3: 
            print('\n|=========| Adjudicar obra |==========|')
            try:
                Obra.adjudicar_obra()
            except:
                print(ERROR_GENERICO)

        case 4: 
            print('\n|=========| Iniciar obra |==========|')
            try:
                Obra.iniciar_obra()
            except:
                print(ERROR_GENERICO)

        case 5:
            print('\n|=========| Actualizar porcentaje de avance |==========|')
            try:
                Obra.actualizar_porcentaje_avance()
            except:
                print(ERROR_GENERICO)

        case 6:
            print('\n|=========| Incrementar plazo |==========|')
            try:
                Obra.incrementar_plazo()
            except:
                print(ERROR_GENERICO)

        case 7: 
            print('\n|=========| Incrementar mano de obra |==========|')
            try:
                Obra.incrementar_mano_obra()
            except:
                print(ERROR_GENERICO)

        case 8:
            print('\n|=========| Finalizar una obra |==========|')
            try:
                Obra.finalizar_obra()
            except:
                print(ERROR_GENERICO)

        case 9:
            print('\n|=========| Rescindir una obra |==========|')
            try:
                Obra.rescindir_obra()
            except:
                print(ERROR_GENERICO)

        case 10:
            print('\n|=========| Indicadores |==========|')
            ejecutar_indicadores()

        case 11:
            print('Saliendo del programa...')
            return

        case _:
            print('# Opción inexistente')

def ejecutar_programa():
    print(TEXTO_ENCABEZADO)
    print(TEXTO_OPCIONES)

    try:
        print('\n|============| Ingresar una opción del 1 al 9 |============|')
        opcion_ingresada = int(input('# Opcion a ingresar: '))
        ejecutar_opcion_ingresada(opcion_ingresada)
    except ValueError:
        print('# Opción invalida')

#ejecutar_programa()