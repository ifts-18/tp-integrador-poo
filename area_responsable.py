from peewee import AutoField, CharField
from base_model import BaseModel

class AreaResponsable(BaseModel):
    id = AutoField()
    nombre = CharField(max_length=100)

    class Meta:
        table_name = 'areas_responsables'

    def ver_areas_responsables():
        areas = AreaResponsable.select(AreaResponsable.nombre)
        nombre_areas = [area_responsable.nombre for area_responsable in areas]
        print('\n|=========|Areas responsables |==========|')
        return nombre_areas
    
    def obtener_areas_responsables():
        areas = []
        areas_en_bd = AreaResponsable.select(AreaResponsable.id, AreaResponsable.nombre)
        for area in areas_en_bd:
            plantilla = {
                'id': area.id,
                'nombre': area.nombre
            }
            areas.append(plantilla)
        return areas
