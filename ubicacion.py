from peewee import AutoField, CharField, ForeignKeyField
from base_model import BaseModel
from barrio import Barrio

class Ubicacion(BaseModel):
    id = AutoField()
    direccion = CharField(max_length=100)
    barrio = ForeignKeyField(Barrio, backref='ubicaciones')

    class Meta:
        table_name = 'ubicaciones'