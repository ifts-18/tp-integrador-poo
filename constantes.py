DATASET = './dataset/observatorio-de-obras-urbanas.csv'

COLUMNAS_ELEGIDAS = [ 
    'entorno', 
    'nombre', 
    'etapa', 
    'tipo', 
    'area_responsable', 
    'descripcion', 
    'monto_contrato', 
    'comuna', 
    'barrio', 
    'direccion', 
    'fecha_inicio', 
    'fecha_fin_inicial', 
    'plazo_meses',
    'porcentaje_avance',
    'licitacion_oferta_empresa',
    'licitacion_anio',
    'contratacion_tipo',
    'nro_contratacion',
    'cuit_contratista',
    'mano_obra',
    'destacada',
    'expediente-numero',
    'financiamiento'
]

COLUMNAS_CON_REGISTROS_NULOS_A_ELIMINAR = [
    'entorno', #0
    'nombre', #1
    'etapa', #2
    'tipo', #3
    'area_responsable',#4
    'descripcion',#5
    'monto_contrato',#6
    'comuna', #7
    'barrio', #8
    'direccion', #9
    'fecha_inicio', #10
    'fecha_fin_inicial',#11
    'plazo_meses',#12
    'porcentaje_avance',#13
    'licitacion_oferta_empresa',#14
    'licitacion_anio',#15
    'contratacion_tipo',#16
    'nro_contratacion',#17
    'cuit_contratista',#18
    'mano_obra',#19
    'destacada'#20
]

VALORES_NULOS_A_REEMPLAZAR = {
    'destacada': 'NO',
    'mano_obra': 0,
    'financiamiento': 'Sin especificar',
    'expediente-numero': 'Sin especificar'
}


BARRIOS = [
    {'opcion': 1, 'nombre' : 'Agronomia'},
    {'opcion': 2, 'nombre' : 'Almagro'},
    {'opcion': 3, 'nombre' : 'Balvanera'},
    {'opcion': 4, 'nombre' : 'Barracas'},
    {'opcion': 5, 'nombre' : 'Belgrano'},
    {'opcion': 6, 'nombre' : 'Boedo'},
    {'opcion': 7, 'nombre' : 'Caballito'},
    {'opcion': 8, 'nombre' : 'Chacarita'},
    {'opcion': 9, 'nombre' : 'Coghlan'},
    {'opcion': 10, 'nombre' : 'Colegiales'},
    {'opcion': 11, 'nombre' : 'Constitucion'},
    {'opcion': 12, 'nombre' : 'Flores'},
    {'opcion': 13, 'nombre' : 'Floresta'},
    {'opcion': 14, 'nombre' : 'La Boca'},
    {'opcion': 15, 'nombre' : 'La Paternal'},
    {'opcion': 16, 'nombre' : 'Liniers'},
    {'opcion': 17, 'nombre' : 'Mataderos'},
    {'opcion': 18, 'nombre' : 'Monte Castro'},
    {'opcion': 19, 'nombre' : 'Montserrat'},
    {'opcion': 20, 'nombre' : 'Nueva Pompeya'},
    {'opcion': 21, 'nombre' : 'Nuñez'},
    {'opcion': 22, 'nombre' : 'Palermo'},
    {'opcion': 23, 'nombre' : 'Parque Avellaneda'},
    {'opcion': 24, 'nombre' : 'Parque Chacabuco'},
    {'opcion': 25, 'nombre' : 'Parque Chas'},
    {'opcion': 26, 'nombre' : 'Parque Patricios'},
    {'opcion': 27, 'nombre' : 'Puerto Madero'},
    {'opcion': 28, 'nombre' : 'Recoleta'},
    {'opcion': 29, 'nombre' : 'Retiro'},
    {'opcion': 30, 'nombre' : 'Saavedra'},
    {'opcion': 31, 'nombre' : 'San Cristobal'},
    {'opcion': 32, 'nombre' : 'San Nicolas'},
    {'opcion': 33, 'nombre' : 'San Telmo'},
    {'opcion': 34, 'nombre' : 'Versalles'},
    {'opcion': 35, 'nombre' : 'Villa Crespo'},
    {'opcion': 36, 'nombre' : 'Villa Devoto'},
    {'opcion': 37, 'nombre' : 'Villa General Mitre'},
    {'opcion': 38, 'nombre' : 'Villa Lugano'},
    {'opcion': 39, 'nombre' : 'Villa Luro'},
    {'opcion': 40, 'nombre' : 'Villa Ortuzar'},
    {'opcion': 41, 'nombre' : 'Villa Pueyrredon'},
    {'opcion': 42, 'nombre' : 'Villa Real'},
    {'opcion': 43, 'nombre' : 'Villa Riachuelo'},
    {'opcion': 44, 'nombre' : 'Villa Santa Rita'},
    {'opcion': 45, 'nombre' : 'Villa Soldati'},
    {'opcion': 46, 'nombre' : 'Villa Urquiza'},
    {'opcion': 47, 'nombre' : 'Villa del Parque'},
    {'opcion': 48, 'nombre' : 'Velez Sarfield'},
]

AREAS_RESPONSABLES = [
    {'opcion': 1, 'nombre': 'Ministerio de Educacion'},
    {'opcion': 2, 'nombre': 'Secretaria de transporte y obras publicas'},
    {'opcion': 3, 'nombre': 'Corporacion Buenos Aires Sur'},
    {'opcion': 4, 'nombre': 'Instituto de la Vivienda'},
    {'opcion': 5, 'nombre': 'Ministerio de salud'},
    {'opcion': 6, 'nombre': 'Subsecretaria de gestion comunal'},
    {'opcion': 7, 'nombre': 'Ministerio de cultura'},
    {'opcion': 8, 'nombre': 'Ministerio de espacio publico e higiene urbana'},
    {'opcion': 9, 'nombre': 'Ministerio de desarrollo humano y habitat'},
    {'opcion': 10, 'nombre': 'Ministerio de justicia y seguridad'},
]

# VER 
TIPOS_DE_OBRAS = [
    {'opcion': 1, 'nombre': 'Escuelas'},
    {'opcion': 2, 'nombre': 'Espacio publico'},
    {'opcion': 3, 'nombre': 'Vivienda'},
    {'opcion': 4, 'nombre': 'Hidraulica e infraestructura'},
    {'opcion': 5, 'nombre': 'Arquitectura'},
    {'opcion': 6, 'nombre': 'Transporte'},
    {'opcion': 7, 'nombre': 'Salud'},
    {'opcion': 8, 'nombre': 'Vivienda nueva'},
    {'opcion': 9, 'nombre': 'Infraestructura'},
    {'opcion': 10, 'nombre': 'Instalaciones'},
    {'opcion': 11, 'nombre': 'Ingenieria'},
]

TIPO_CONTRATACION = [
    {'opcion': 1, 'nombre': 'Licitacion publica'},
    {'opcion': 2, 'nombre': 'Contratacion directa'},
    {'opcion': 3, 'nombre': 'Licitacion privada'},
    {'opcion': 4, 'nombre': 'Licitacion privada de obra menor'},
    {'opcion': 5, 'nombre': 'Obra menor'},
    {'opcion': 6, 'nombre': 'Sin efecto'},
    {'opcion': 7, 'nombre': 'Contratacion mantenimiento'},
    {'opcion': 8, 'nombre': 'Contratacion de varias empresas'},
    {'opcion': 9, 'nombre': 'Convenio'},
    {'opcion': 10, 'nombre': 'Obra de emergencia'},
    {'opcion': 11, 'nombre': 'Donacion'},
]

TEXTO_ENCABEZADO = '''
|==========================================================|
|---------------| BASE DE DATOS: OBRAS     |---------------|
|==========================================================|
'''

TEXTO_OPCIONES = '''
|==========================================================|
|-----------------| Opciones disponibles |-----------------|
|----------------------------------------------------------|
| - Opción 1: Nuevo proyecto                               |
| - Opción 2: Iniciar contratación                         |
| - Opción 3: Adjudicar obra                               |
| - Opción 4: Iniciar obra                                 |
| - Opción 5: Actualizar porcentaje avance                 |
| - Opción 6: Incrementar plazo                            |
| - Opción 7: Incrementar mano obra                        |
| - Opción 8: Finalizar obra                               |
| - Opción 9: Rescindir obra                               |
| - Opción 10: Ejecutar indicadores                        |
| - Opción 11: Salir de programa                           |
|----------------------------------------------------------|
'''

ERROR_GENERICO = '# Operación fallida, intente nuevamente'