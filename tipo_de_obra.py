from peewee import AutoField, CharField
from base_model import BaseModel

class TipoDeObra(BaseModel):
    id = AutoField()
    nombre = CharField(max_length=100)

    class Meta:
        table_name = 'tipos_de_obra'

    def ver_tipo_de_obras():
        tipos_de_obras = TipoDeObra.select(TipoDeObra.nombre)
        nombre_tipos = [tipo.nombre for tipo in tipos_de_obras ]
        print('\n|=========| Tipos de obras |==========|')
        return nombre_tipos

    def obtener_tipos_de_obra():
        tipos_de_obra = []
        tipos_de_obra_en_bd = TipoDeObra.select(TipoDeObra.id, TipoDeObra.nombre)
        for tipo_de_obra in tipos_de_obra_en_bd:
            plantilla = {
                'id': tipo_de_obra.id,
                'nombre': tipo_de_obra.nombre
            }
            tipos_de_obra.append(plantilla)
        return tipos_de_obra
